# Changelog

## Unreleased
### Added
- Capex calculation for new vehicles
- Correct calibration and aggregation of GHG emissions

## 1.5
### Added
- Disaggregation in urban/non urban and last mile/non last mile
- Adding active motorised and active non motorised in transportation modes


## 1.4
### Added
- C02e emissions by fuel and by mode
- naming conventions for the columns according to the cube
### Changed
